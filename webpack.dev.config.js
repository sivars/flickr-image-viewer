const EXTRACT_TEXT_PLUGIN = require('extract-text-webpack-plugin');
const PATH = require('path');
const WEBPACK = require('webpack');
const COPY_WEBPACK_PLUGIN = require('copy-webpack-plugin');

module.exports = {
  context: PATH.resolve('src'),

  entry: {
    javascript: './scripts/app.js',
    html: './index.html',
  },
  output: {
    filename: 'app.js',
    path: PATH.resolve('dist'),
  },
  resolve: {
    root: [PATH.resolve(__dirname, 'app'), PATH.resolve(__dirname, 'node_modules')]
  },
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader'
      }
    ],
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['babel-loader'],
      }, {
        test: /\.scss$/,
        loader: EXTRACT_TEXT_PLUGIN.extract('css!sass')
      }, {
        test: /\.html$/,
        loader: 'file?name=[name].[ext]',
      }, {
        test: /\.json$/,
        loader: 'file?name=[name].[ext]',
      }, {
        test: /\.(jpg|jpeg|png|gif)$/,
        loader: 'file?name=[name].[ext]',
      }, {
        test: /\.(ttf|eot|svg|woff|woff(2))(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file?name=fonts/[name].[ext]'
      }
    ]
  },
  devServer: {
    contentBase: './dist',
    historyApiFallback: true
  },
  eslint: {
    configFile: './.eslintrc'
  },
  plugins: [
    new EXTRACT_TEXT_PLUGIN('style.css', {
      allChunks: true
    }),
    new WEBPACK.optimize.CommonsChunkPlugin('common.js'),
    new COPY_WEBPACK_PLUGIN([
      { from: 'static', to: 'static' },
      { from: 'pwa' }
    ])
  ]
};
