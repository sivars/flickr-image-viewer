/* eslint-disable*/
importScripts('/cache-polyfill.js');

self.addEventListener('install', function(e) {
  e.waitUntil(
   caches.open('flickr-1.0.0').then(function(cache) {
     return cache.addAll([
       '/',
       '/index.html',
       '/offline.html',
       '/style.css',
       '/fonts/fontawesome-webfont.eot',
       '/fonts/fontawesome-webfont.svg',
       '/fonts/fontawesome-webfont.ttf',
       '/fonts/fontawesome-webfont.woff',
       '/fonts/fontawesome-webfont.woff2',
       '/common.js',
       '/app.js'
     ]);
   })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    }).catch(function() {
      return caches.match('/offline.html');
    })
  );
});
