/**
 * Slider component. Shows images in slider style
 * @function
 * @param {string} containerId - Id for the container div which encloses page number buttons.
 * @param {Object[]} imageObjectArray - Array of image objects
 * @param {string} urlKey - Key to access the image url from the above image object
 * @param {Object} options - Extra options to be passed into the function
 * @param {array} options.custom_attributes_key - Attributes from the image object
 *  which needs to be set on each image tag. Useful for knowing more info about image
 * @param {function} options.btnClickCallback - Function to be called when
 *  next or previous button is clicked
 * @returns {Object} contains helper functions to access slider methods
 */

// Import styles for this component
import '../styles/components/_slider.scss';

const SLIDER_IMAGE_CLASS = 'slider__img';
const SLIDER_IMAGE_ACTIVE_CLASS = 'slider__img--active';
const SLIDER_BTN_CLASS = 'slider__btn';
const SLIDER_ICON_CLASS = 'slider__icon';
const SLIDER_BTN_LEFT_CLASS = 'slider__btn--left';
const SLIDER_BTN_RIGHT_CLASS = 'slider__btn--right';
const PREVIOUS_DATA_ATTRIBUTE = 'previous';
const NEXT_DATA_ATTRIBUTE = 'next';

export default function slider(containerId = '', imageObjectArray = [], urlKey, options = {}) {
  // create the container div
  const dContainerDiv = document.createElement('div');
  dContainerDiv.setAttribute('id', containerId);
  dContainerDiv.setAttribute('class', 'slider');
  dContainerDiv.setAttribute('role', 'listbox');
  dContainerDiv.setAttribute('aria-label', 'Full Size Images Carousel');

  let currentIndex = 1;
  let currentImage;
  // Get total images
  let totalImages = imageObjectArray.length;
  const imageElementsArray = [];

  let dImageElement;

  // Helper function to update the image div
  // with details like url, and custom attributes
  const updateImageDiv =
    function updateImageDiv(domElement, imageObj, index, updatedUrlKey, customAttrs) {
      const tempElement = domElement;
      tempElement.style.backgroundImage = `url(${imageObj[updatedUrlKey]})`;
      if (!index) {
        tempElement.setAttribute('class', `${SLIDER_IMAGE_CLASS} ${SLIDER_IMAGE_ACTIVE_CLASS}`);
        currentImage = tempElement;
      } else {
        tempElement.setAttribute('class', SLIDER_IMAGE_CLASS);
      }
      tempElement.setAttribute('data-index', index + 1);
      if (customAttrs) {
        customAttrs.map((attrs) => {
          tempElement.setAttribute(`data-${attrs}`, imageObj[attrs]);
          return attrs;
        });
      }
      return tempElement;
    };

  // Create an img tag for each element in the image object array
  imageObjectArray.map((item, index) => {
    dImageElement = document.createElement('div');
    dImageElement =
      updateImageDiv(dImageElement, item, index, urlKey, options.custom_attributes_key);

    imageElementsArray.push(dImageElement);
    dContainerDiv.appendChild(dImageElement);
    return dImageElement;
  });

  // Create the previous button
  const dPrevBtn = document.createElement('button');
  dPrevBtn.setAttribute('class', `${SLIDER_BTN_CLASS} ${SLIDER_BTN_LEFT_CLASS}`);
  dPrevBtn.setAttribute('data-type', PREVIOUS_DATA_ATTRIBUTE);
  dPrevBtn.setAttribute('tabIndex', '0');
  dPrevBtn.setAttribute('role', 'button');
  dPrevBtn.setAttribute('aria-label', 'Previous Carousel Image');
  dPrevBtn.setAttribute('disabled', '');
  // Create the previous icon
  const dPrevIcon = document.createElement('i');
  dPrevIcon.setAttribute('class', `fa fa-angle-left ${SLIDER_ICON_CLASS}`);
  dPrevIcon.setAttribute('data-type', PREVIOUS_DATA_ATTRIBUTE);
  dPrevBtn.appendChild(dPrevIcon);

  dContainerDiv.appendChild(dPrevBtn);


  // Create the next button
  const dNextBtn = document.createElement('button');
  dNextBtn.setAttribute('class', `${SLIDER_BTN_CLASS} ${SLIDER_BTN_RIGHT_CLASS}`);
  dNextBtn.setAttribute('data-type', NEXT_DATA_ATTRIBUTE);
  dNextBtn.setAttribute('tabIndex', '0');
  dNextBtn.setAttribute('role', 'button');
  dNextBtn.setAttribute('aria-label', 'Next Carousel Image');
  if (totalImages < 2) {
    dNextBtn.setAttribute('disabled', '');
  }
  // Create the next icon
  const dNextIcon = document.createElement('i');
  dNextIcon.setAttribute('class', `fa fa-angle-right ${SLIDER_ICON_CLASS}`);
  dNextIcon.setAttribute('data-type', NEXT_DATA_ATTRIBUTE);
  dNextBtn.appendChild(dNextIcon);

  dContainerDiv.appendChild(dNextBtn);

  // Returns the container element
  const getContainerElement = function getContainerElement() {
    return dContainerDiv;
  };

  // Function to show a specific image in a slider
  const showImage = function showImage(findIndex) {
    let imgElements = dContainerDiv.getElementsByClassName(SLIDER_IMAGE_CLASS);
    imgElements = Array.prototype.slice.call(imgElements);
    imgElements.map((item, index) => {
      item.setAttribute('class', SLIDER_IMAGE_CLASS);
      if (parseInt(findIndex, 10) === (index + 1)) {
        currentIndex = index + 1;
        item.setAttribute('class', `${SLIDER_IMAGE_CLASS} ${SLIDER_IMAGE_ACTIVE_CLASS}`);
        currentImage = item;
        dPrevBtn.removeAttribute('disabled');
        dNextBtn.removeAttribute('disabled');
        if (currentIndex === 1) {
          dPrevBtn.setAttribute('disabled', '');
        }
        if (currentIndex === totalImages) {
          dNextBtn.setAttribute('disabled', '');
        }
      }
      return item;
    });
  };

  // Function to update images in slider
  const updateImages = function updateImages(updatedImageObjArray = []) {
    // Change the current index and total images
    currentIndex = 1;
    dPrevBtn.setAttribute('disabled', '');
    totalImages = updatedImageObjArray.length;
    if (totalImages < 2) {
      dNextBtn.setAttribute('disabled', '');
    } else {
      dNextBtn.removeAttribute('disabled');
    }
    // Update the image divs
    updatedImageObjArray.map((item, index) => {
      updateImageDiv(imageElementsArray[index], item, index, urlKey, options.custom_attributes_key);
      return item;
    });
    return imageElementsArray;
  };

  // On clicking previous or next button show corresponding image
  // After that call the call back function
  dContainerDiv.addEventListener('click', (event) => {
    const btnType = event.target.getAttribute('data-type');
    if (btnType === PREVIOUS_DATA_ATTRIBUTE || btnType === NEXT_DATA_ATTRIBUTE) {
      if (btnType === PREVIOUS_DATA_ATTRIBUTE && currentIndex !== 1) {
        showImage(currentIndex - 1);
      } else if (btnType === NEXT_DATA_ATTRIBUTE && currentIndex !== totalImages) {
        showImage(currentIndex + 1);
      }
      return options && options.btnClickCallback && options.btnClickCallback(btnType, currentImage);
    }
  });
  return {
    getContainerElement,
    showImage,
    updateImages
  };
}
