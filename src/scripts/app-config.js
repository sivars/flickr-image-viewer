// Config for the app
const config = {
  DEFAULT_SEARCH_TERM: 'nature',
  IMAGES_PER_PAGE: 15,
  MAX_PAGES: 6
};
export default config;
