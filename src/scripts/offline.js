/**
 * Used to set the no results
 * @function
 * @param {string} containerId - Id for the container div which encloses page number buttons.
 * @returns {Object} contains helper functions to access no screen methods
 */
import '../styles/components/_offline.scss';

const OFFLINE_TEXT = 'Oh ho! Looks like you are offline. Please connect to the internet and try again';
const CONTAINER_CLASS = 'offline';
const TEXT_CLASS = 'offline__text';

export default function offline(containerId = '') {
  const dContainerDiv = document.createElement('div');
  dContainerDiv.setAttribute('id', containerId);
  dContainerDiv.setAttribute('class', CONTAINER_CLASS);

  const dTextElement = document.createElement('h1');
  dTextElement.innerHTML = OFFLINE_TEXT;
  dTextElement.setAttribute('class', TEXT_CLASS);

  dContainerDiv.appendChild(dTextElement);

  // Function to return the container element
  const getContainerElement = function getContainerElement() {
    return dContainerDiv;
  };

  // Function to show the no results screen
  const showOffline = function showNoResults() {
    dContainerDiv.style.display = 'block';
  };

  // Function to hide the no results screen
  const hideOffline = function hideNoResults() {
    dContainerDiv.style.display = 'none';
  };
  return {
    getContainerElement,
    showOffline,
    hideOffline
  };
}
