/**
 * Used to set thumbnail from the given set of images
 * @function
 * @param {string} containerId - Id for the container div which encloses thumbnails.
 * @param {Object[]} imageObjectArray - Collection of image objects where each object contains
 *    info about the image to load
 * @param {string} urlKey - Key to be used in the above image object array for loading images
 * @param {Object} options - Extra options to be passed into the function
 * @param {string[]} options.custom_attributes_key - Array of custom attribuets that needs to be
 *    added to image tags
 * @param {function} options.imgClickCallback - Function to be called when
 *  a thumbnail image is clicked
 * @returns {Object} returns an object which contains helper functions for accessing thumbnails
 */

// Styles for the component
import '../styles/components/_thumbnail.scss';

const THUMB_IMAGE_CLASS = 'thumbnail__img';
const THUMB_IMAGE_ACTIVE_CLASS = 'thumbnail__img--active';

export default function thumbnail(containerId = '', imageObjectArray = [], urlKey, options = {}) {
  // Container div which encloses image tags
  const dContainerDiv = document.createElement('div');
  dContainerDiv.setAttribute('id', containerId);
  dContainerDiv.setAttribute('class', 'thumbnail');
  dContainerDiv.setAttribute('role', 'listbox');
  dContainerDiv.setAttribute('aria-lable', 'Thumbnail Images List');

  let dImageElement;
  const imageElementsArray = [];

  const updateImageElement =
    function updateImageElement(domElement, imageObj, index, updatedUrlKey, customAttrs) {
      const tempElement = domElement;
      tempElement.src = imageObj[updatedUrlKey];
      tempElement.setAttribute('data-index', index + 1);
      tempElement.setAttribute('tabIndex', '0');
      tempElement.setAttribute('role', 'button');
      tempElement.setAttribute('aria-label', 'Thumbnail image');
      if (!index) {
        tempElement.setAttribute('class', `${THUMB_IMAGE_CLASS} ${THUMB_IMAGE_ACTIVE_CLASS}`);
      } else {
        tempElement.setAttribute('class', THUMB_IMAGE_CLASS);
      }
      // Set custom attributes for image tag
      // const customAttributes = options.custom_attributes_key;
      if (customAttrs) {
        customAttrs.map((attrs) => {
          tempElement.setAttribute(`data-${attrs}`, imageObj[attrs]);
          return attrs;
        });
      }
      return tempElement;
    };
  // Iterate through image object array items
  imageObjectArray.map((item, index) => {
    // Create image element
    dImageElement = document.createElement('img');
    // Set the src for image tag
    dImageElement =
      updateImageElement(dImageElement, item, index, urlKey, options.custom_attributes_key);
    // Attach the image div to the container div
    imageElementsArray.push(dImageElement);
    dContainerDiv.appendChild(dImageElement);
    return dImageElement;
  });

  // Function to return the container div
  const getContainerElement = function getContainerElement() {
    return dContainerDiv;
  };

  // Function to activate a specific image
  const activateImage = function activateImage(findIndex) {
    let dImgElements = dContainerDiv.getElementsByClassName(THUMB_IMAGE_CLASS);
    dImgElements = Array.prototype.slice.call(dImgElements);
    dImgElements.map((item, index) => {
      item.setAttribute('class', THUMB_IMAGE_CLASS);
      if (parseInt(findIndex, 10) === index + 1) {
        item.setAttribute('class', `${THUMB_IMAGE_CLASS} ${THUMB_IMAGE_ACTIVE_CLASS}`);
      }
      return item;
    });
  };

  // Function to update images in thumbnails
  const updateImages = function updateImages(updatedImageObjArray = []) {
    updatedImageObjArray.map((item, index) => {
      updateImageElement(
        imageElementsArray[index], item, index, urlKey, options.custom_attributes_key);
      return item;
    });
    return imageElementsArray;
  };
  // Helper function to select thumbnail based on event occured
  const selectImage = function selectImage(event, checkEnterKey) {
    const dTargetElem = event.target;
    let keyPressChecked = true;
    if (checkEnterKey) {
      keyPressChecked = (event.which === 13 || event.keyCode === 13);
    }
    if (dTargetElem.tagName === 'IMG' && dTargetElem.getAttribute('data-index') && keyPressChecked) {
      activateImage(dTargetElem.getAttribute('data-index'));
      if (options.imgClickCallback) {
        options.imgClickCallback(dTargetElem);
      }
    }
  };
  // On click of thumbnail set the image to active
  // And then call the call back function
  dContainerDiv.addEventListener('click', (event) => {
    selectImage(event);
  });

  dContainerDiv.addEventListener('keydown', (event) => {
    selectImage(event, true);
  });
  return {
    getContainerElement,
    activateImage,
    updateImages
  };
}
