/**
 * Main app.
 */

// Import main style sheet
import '../styles/style.scss';

// Import necessary modules
import flickr from './flickr';
import slider from './slider';
import thumbnail from './thumbnail';
import pagination from './pagination';
import search from './search';
import photoCredit from './photo-credit';
import noResults from './no-results';
import loading from './loading';
import offline from './offline';

// Import the app config
import appConfig from './app-config';
import './pwa';

// Get the required containers
const dSliderContainer = document.getElementById('slider-container__slider');
const dThumbnailContainer = document.getElementById('thumbnail-container');
const dPaginationContainer = document.getElementById('pagination-container');
const dSearchContainer = document.getElementById('search-container');
const dCreditContainer = document.getElementById('credit-container');
const dLoadingContainer = document.getElementById('loading-container');
const dNoResultsContainer = document.getElementById('no-results-container');
const dOfflineContainer = document.getElementById('offline-container');

// Initialize the flickr module with default search term and images per page
const flickrObj = flickr({ searchTerm: appConfig.DEFAULT_SEARCH_TERM,
  imagesPerPage: appConfig.IMAGES_PER_PAGE });
// Configure the number of pages shown
const MAX_PAGES = appConfig.MAX_PAGES;

let sliderObj;
let thumbnailObj;

// Create the photographer credit module and append it to the div
const photoCreditObj = photoCredit('photo-credit');
dCreditContainer.appendChild(photoCreditObj.getContainerElement());

// Create the no results module and append it to div
const noResultsObj = noResults('no-results');
dNoResultsContainer.appendChild(noResultsObj.getContainerElement());

// Create the loading module and append it to div
const loadingObj = loading('loading');
dLoadingContainer.appendChild(loadingObj.getContainerElement());

const offlineObj = offline('offline');
dOfflineContainer.appendChild(offlineObj.getContainerElement());

// Function to be called when a thumbnail is clicked
const thumbnailClick = function thumbnailClick(imgNode) {
  sliderObj.showImage(imgNode.getAttribute('data-index'));
  photoCreditObj.setName(imgNode.getAttribute('data-ownername'));
};

// Function to be called when slider's next or precious is clicked
const sliderBtnClick = function sliderBtnClick(type, imgNode) {
  thumbnailObj.activateImage(imgNode.getAttribute('data-index'));
  photoCreditObj.setName(imgNode.getAttribute('data-ownername'));
};

// Main function to get the images and update the DOM
const getImages = function getImages(pageNumber, updatePagination) {
  // Helper function to be called when paginate button is clicked
  const paginateBtnClick = function paginateBtnClick(btnNode) {
    const isNavBtn = btnNode.getAttribute('data-navigate-btn');
    if (btnNode.tagName === 'BUTTON' && btnNode.getAttribute('data-index')) {
      getImages(parseInt(btnNode.getAttribute('data-index'), 10), isNavBtn);
    }
  };
  // Show the loading screen and hide the no results message
  loadingObj.showLoading();
  noResultsObj.hideNoResults();
  offlineObj.hideOffline();

  // Get the page from the flickr module
  // and update the thumbnail and slider
  flickrObj.getPage(pageNumber).then((response) => {
    // Update photogrpaher credit
    if (response && response.length) {
      photoCreditObj.setName(response[0].ownername);
    } else {
      noResultsObj.showNoResults();
    }
    // Update thumbnail
    if (!thumbnailObj) {
      dThumbnailContainer.innerHTML = '';
      thumbnailObj = thumbnail('thumbnail', response, 'url_q', { custom_attributes_key: ['url_l', 'ownername'], imgClickCallback: thumbnailClick });
      dThumbnailContainer.appendChild(thumbnailObj.getContainerElement());
    } else {
      thumbnailObj.updateImages(response);
    }

    // Update slider
    if (!sliderObj) {
      dSliderContainer.innerHTML = '';
      sliderObj = slider('slider', response, 'url_l', { custom_attributes_key: ['url_l', 'ownername'], btnClickCallback: sliderBtnClick });
      dSliderContainer.appendChild(sliderObj.getContainerElement());
    } else {
      sliderObj.updateImages(response);
    }

    // Update pagination
    if (updatePagination) {
      let pageNumberCopy = pageNumber;
      flickrObj.getTotalPages().then((totalPages) => {
        if (pageNumberCopy === totalPages) {
          pageNumberCopy = (pageNumberCopy - MAX_PAGES) + 1;
        }
        const paginationObj = pagination('paginate', pageNumberCopy, MAX_PAGES, totalPages, { btnClickCallback: paginateBtnClick });
        dPaginationContainer.innerHTML = '';
        dPaginationContainer.appendChild(paginationObj.getContainerElement());
      });
    }
    loadingObj.hideLoading();
  }).catch((error) => {
    loadingObj.hideLoading();
    if (error === 'Offline') {
      offlineObj.showOffline();
    }
  });
};

// Function to be called when search is clicked
const searchSubmit = function searchSubmit(text) {
  if (text.trim()) {
    flickrObj.setSearchTerm(text);
    getImages(1, true);
  }
};

// Create and attach the search module
const searchObj = search('search', { submitCallback: searchSubmit });
dSearchContainer.appendChild(searchObj.getContainerElement());

// Get the Flickr images
getImages(1, true);
