/**
 * Used to set the loading screen
 * @function
 * @param {string} containerId - Id for the container div which encloses page number buttons.
 * @returns {Object} contains helper functions to access loading methods
 */
import '../styles/components/_loading.scss';

const LOADING_TEXT = 'Loading results. Please wait...';
const CONTAINER_CLASS = 'loading';
const TEXT_CLASS = 'loading__text';

export default function loading(containerId = '') {
  const dContainerDiv = document.createElement('div');
  dContainerDiv.setAttribute('id', containerId);
  dContainerDiv.setAttribute('class', CONTAINER_CLASS);

  const dTextElement = document.createElement('h1');
  dTextElement.innerHTML = LOADING_TEXT;
  dTextElement.setAttribute('class', TEXT_CLASS);

  dContainerDiv.appendChild(dTextElement);

  // Function to return the container element
  const getContainerElement = function getContainerElement() {
    return dContainerDiv;
  };

  // Function to show the loading screen
  const showLoading = function showNoResults() {
    dContainerDiv.style.display = 'block';
  };

  // Function to hide the loading screen
  const hideLoading = function hideNoResults() {
    dContainerDiv.style.display = 'none';
  };
  return {
    getContainerElement,
    showLoading,
    hideLoading
  };
}
