/**
 * Used to set the no results
 * @function
 * @param {string} containerId - Id for the container div which encloses page number buttons.
 * @returns {Object} contains helper functions to access no screen methods
 */
import '../styles/components/_no-results.scss';

const NO_RESULTS_TEXT = 'No results found :( Please try a different search query';
const CONTAINER_CLASS = 'no-results';
const TEXT_CLASS = 'no-results__text';

export default function noResults(containerId = '') {
  const dContainerDiv = document.createElement('div');
  dContainerDiv.setAttribute('id', containerId);
  dContainerDiv.setAttribute('class', CONTAINER_CLASS);

  const dTextElement = document.createElement('h1');
  dTextElement.innerHTML = NO_RESULTS_TEXT;
  dTextElement.setAttribute('class', TEXT_CLASS);

  dContainerDiv.appendChild(dTextElement);

  // Function to return the container element
  const getContainerElement = function getContainerElement() {
    return dContainerDiv;
  };

  // Function to show the no results screen
  const showNoResults = function showNoResults() {
    dContainerDiv.style.display = 'block';
  };

  // Function to hide the no results screen
  const hideNoResults = function hideNoResults() {
    dContainerDiv.style.display = 'none';
  };
  return {
    getContainerElement,
    showNoResults,
    hideNoResults
  };
}
