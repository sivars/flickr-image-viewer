/**
 * Search component
 * @function
 * @param {string} containerId - Id for the container div which encloses page number buttons.
 * @param {Object} options - Extra options to be passed into the function
 * @param {string} options.submitCallback - Call back function to be called on submit
 * @returns {Object} returns an object which contains helper functions for accessing module
 */

import '../styles/components/_search.scss';

const SEARCH_CLASS = 'search';
const SEARCH_INPUT_CLASS = 'search__input';
const SEARCH_ICON_CLASS = 'search__icon';

export default function search(containerId = '', options) {
  // Create a container div
  const dContainerDiv = document.createElement('div');
  dContainerDiv.setAttribute('id', containerId);
  dContainerDiv.setAttribute('class', SEARCH_CLASS);

  // Create the text element
  const dInput = document.createElement('input');
  dInput.setAttribute('type', 'text');
  dInput.setAttribute('placeholder', 'search');
  dInput.setAttribute('class', SEARCH_INPUT_CLASS);
  dInput.setAttribute('tabIndex', '0');
  dInput.setAttribute('autofocus', '');
  dInput.setAttribute('aria-label', 'Search Images Textbox');

  dContainerDiv.appendChild(dInput);

  // Create a search button
  const searchBtn = document.createElement('i');
  searchBtn.setAttribute('class', `fa fa-search ${SEARCH_ICON_CLASS}`);
  searchBtn.setAttribute('data-role', 'search');
  searchBtn.setAttribute('aria-label', 'Submit Button');

  dContainerDiv.appendChild(searchBtn);

  // Function to return the container element
  const getContainerElement = function getContainerElement() {
    return dContainerDiv;
  };

  // On clicking of search button, call the submit call back function
  dContainerDiv.addEventListener('click', (event) => {
    if (event.target.tagName === 'I' && event.target.getAttribute('data-role') === 'search') {
      return options && options.submitCallback && options.submitCallback(dInput.value);
    }
  });

  // Call the submit call back function when enter key is pressed in text box
  dInput.addEventListener('keydown', (event) => {
    if (event.which === 13 || event.keyCode === 13) {
      return options && options.submitCallback && options.submitCallback(dInput.value);
    }
  });
  return {
    getContainerElement
  };
}
