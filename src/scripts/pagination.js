/**
 * Used to set page number buttons. This is a stateless component.
 * @function
 * @param {string} containerId - Id for the container div which encloses page number buttons.
 * @param {number} startIndex - Starting index for the button
 * @param {number} pagesToShow - Number of pages to show
 * @param {number} lastIndex - Last page number in the collection
 * @param {Object} options - Extra options to be passed into the function
 * @returns {Object} contains helper functions to access pagination methods
 */
import '../styles/components/_pagination.scss';

// CSS class names for the pagination button and active state
const PAGINATION_BTN_CLASS = 'pagination__btn';
const PAGINATION_BTN_ACTIVE_CLASS = 'pagination__btn--active';

export default function pagination(containerId = '', startIndex, pagesToShow, lastIndex, options) {
  // Create the container div and set id
  const dContainerDiv = document.createElement('div');
  dContainerDiv.setAttribute('id', containerId);

  // Navigation buttons like previous page, next, first and last page
  const navigateBtns = [{
    html: '<',
    index: startIndex - pagesToShow,
    disabled: (startIndex === 1),
    prepend: true,
    label: 'Previous Page',
  }, {
    html: '<<',
    index: 1,
    disabled: (startIndex === 1),
    prepend: true,
    label: 'First Page',
  }, {
    html: '>',
    index: startIndex + pagesToShow,
    disabled: (startIndex === lastIndex || startIndex + pagesToShow === lastIndex + 1),
    append: true,
    label: 'Next Page',
  }, {
    html: '>>',
    index: lastIndex,
    disabled: (startIndex === lastIndex || startIndex + pagesToShow === lastIndex + 1),
    append: true,
    label: 'Last Page'
  }];

  let dPageBtn;
  // Calculate the upper limit based on last Index
  const upperLimit = (startIndex + pagesToShow) > lastIndex ? lastIndex :
   ((startIndex + pagesToShow) - 1);

  // Create buttons and attach to container div
  for (let i = startIndex; i <= upperLimit; i += 1) {
    dPageBtn = document.createElement('button');
    dPageBtn.innerHTML = i;
    // Set the index value on the data attribute
    dPageBtn.setAttribute('data-index', i);
    dPageBtn.setAttribute('tabIndex', '0');
    dPageBtn.setAttribute('role', 'button');
    dPageBtn.setAttribute('aria-label', 'Pagination button');
    if (startIndex === i) {
      dPageBtn.setAttribute('class', `${PAGINATION_BTN_CLASS} ${PAGINATION_BTN_ACTIVE_CLASS}`);
      dPageBtn.setAttribute('data-selected', true);
    } else {
      dPageBtn.setAttribute('class', PAGINATION_BTN_CLASS);
    }
    dContainerDiv.appendChild(dPageBtn);
  }

  // Create the navigation buttons and attach to container div
  let dExtraBtn;
  navigateBtns.map((item) => {
    dExtraBtn = document.createElement('button');
    dExtraBtn.innerHTML = item.html;
    dExtraBtn.setAttribute('class', PAGINATION_BTN_CLASS);
    // Set the index value on the data attribute
    dExtraBtn.setAttribute('data-index', item.index);
    dExtraBtn.setAttribute('tabIndex', 0);
    dExtraBtn.setAttribute('role', 'button');
    dExtraBtn.setAttribute('aria-label', item.label);
    // Set navigate-btn attribute. Used to differentiate between
    // normal buttons with number and previous, next buttons
    dExtraBtn.setAttribute('data-navigate-btn', true);
    if (item.disabled) {
      dExtraBtn.setAttribute('disabled', '');
    }
    if (item.append) {
      dContainerDiv.appendChild(dExtraBtn);
    }
    if (item.prepend) {
      dContainerDiv.insertBefore(dExtraBtn, dContainerDiv.firstChild);
    }
    return dExtraBtn;
  });

  // Set the active class on the button with the given index
  const setActiveButton = function setActiveButton(findIndex) {
    let btns = dContainerDiv.getElementsByClassName(PAGINATION_BTN_CLASS);
    btns = Array.prototype.slice.call(btns);
    btns.map((item) => {
      item.setAttribute('class', PAGINATION_BTN_CLASS);
      item.removeAttribute('data-selected');
      if (parseInt(findIndex, 10) === parseInt(item.getAttribute('data-index'), 10)) {
        item.setAttribute('class', `${PAGINATION_BTN_CLASS} ${PAGINATION_BTN_ACTIVE_CLASS}`);
        item.setAttribute('data-selected', true);
      }
      return item;
    });
  };

  // Get the enclosing container element
  const getContainerElement = function getContainerElement() {
    return dContainerDiv;
  };

  // Event listener added to container instead of buttons to prevent multiple event listeners
  // On clicking, check the type of button and change the active button
  // And then call the callback method passed while intializing
  dContainerDiv.addEventListener('click', (event) => {
    const updatePagination = event.target.getAttribute('data-navigate-btn');
    if (event.target.tagName === 'BUTTON' && event.target.getAttribute('data-index') && !event.target.getAttribute('data-selected')) {
      if (!updatePagination) {
        setActiveButton(event.target.getAttribute('data-index'));
      }
      return options && options.btnClickCallback && options.btnClickCallback(event.target);
    }
  });
  return {
    getContainerElement,
    setActiveButton
  };
}
