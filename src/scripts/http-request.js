/**
 * A Promise wrapped XML Http request
 * @function
 * @param {string} url - URL to intiate a request
 * @returns {Object} returns an object which contains helper functions
 *  for get, post, put, delete methods
 */
export default function httpRequest(url) {
  // Ajax call wrapped around promise
  const ajaxCall = (method, args) => {
    const promise = new Promise((resolve, reject) => {
      let uri = url;
      // Add args to url only if the method is post or put
      if (args && (method === 'POST' || method === 'PUT')) {
        uri += '?';
        let argcount = 0;
        Object.keys(args).forEach((key) => {
          if ({}.hasOwnProperty.call(args, key)) {
            if (argcount) {
              uri += '&';
            }
            argcount += 1;
            uri += `${encodeURIComponent(key)}=${encodeURIComponent(args[key])}`;
          }
        });
      }
      // The actual AJAX request
      const xmlHttp = new XMLHttpRequest();
      xmlHttp.onload = () => {
        if (xmlHttp.status >= 200 && xmlHttp.status < 300) {
          // Performs the function "resolve" when this.status is equal to 2xx
          resolve(xmlHttp.response);
        } else {
          // Performs the function "reject" when this.status is different than 2xx
          reject(xmlHttp.statusText);
        }
      };
      // Reject on error
      xmlHttp.onerror = () => {
        reject(xmlHttp.statusText || 'Offline');
      };
      xmlHttp.open(method, uri);
      xmlHttp.send();
    });
    return promise;
  };
  return {
    get: args => (ajaxCall('GET', url, args)),
    post: args => (ajaxCall('POST', url, args)),
    put: args => (ajaxCall('PUT', url, args)),
    delete: args => (ajaxCall('DELETE', url, args))
  };
}
