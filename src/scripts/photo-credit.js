/**
 * Stateless component to show the photographer credit
 * @function
 * @param {string} containerId - Id for the container div which encloses photographer credit.
 * @returns {Object} contains helper functions to access the module
 */

const PHOTOGRAPHER_CREDIT_TEXT = 'Photographer Credit: ';

export default function photoCredit(containerId = '') {
  // Create the enclosing container div
  const dContainerDiv = document.createElement('div');
  dContainerDiv.setAttribute('id', containerId);

  // Create a span for the text photographer credit
  const dTitleSpan = document.createElement('span');
  dTitleSpan.innerHTML = PHOTOGRAPHER_CREDIT_TEXT;
  dContainerDiv.appendChild(dTitleSpan);

  // Create a span for the actual photographer name
  const dNameSpan = document.createElement('span');
  dNameSpan.innerHTML = '';
  dContainerDiv.appendChild(dNameSpan);

  // Function to get the enclosing container div
  const getContainerElement = function getContainerElement() {
    return dContainerDiv;
  };

  // Function to set the name
  const setName = function setName(name) {
    dNameSpan.innerHTML = name;
  };
  return {
    getContainerElement,
    setName
  };
}
