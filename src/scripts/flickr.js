/**
 * Flickr module. Used to get images from Flickr. This module is independent of DOM
 * @function
 * @param {Object} options - Contains options to be passed into Flickr Module
 * @param {string} options.searchTerm - Search term to filter images from Flickr API
 * @param {number} options.imagesPerPage - Number of images to be returned in a page
 * @returns {Object} returns an object which contains helper functions for accessing flickr methods
 */

// XML Http request wrapped in a promise
import httpRequest from './http-request';

// Default value of images per page
const IMAGES_PER_PAGE = 10;
// Due to the constraint in Flickr API,
// the API can return only first 4000 images for any search term
const MAX_IMAGE_RESULTS = 4000;
// Maximum number of pages possible because of the constraint above
let MAX_PAGES_POSSIBLE = parseInt((MAX_IMAGE_RESULTS / IMAGES_PER_PAGE), 10);

// Default values for the Flickr API module
// Based on the project needs, values from the below object can be
// exposed via an API from this module.
const FLICKR_API = {
  url: 'https://api.flickr.com/services/rest/?method=',
  key: '0916e2c9f43b32b23f7e0349e6f59692',
  // List of methods that can be called on Flickr API
  // search - Useful for searching images based on given search term
  // getRecent - Useful to get recent images when no search term is provided
  methods: {
    search: 'flickr.photos.search',
    getRecent: 'flickr.photos.getRecent'
  },
  // Setting for the type of result that we want from Flickr API
  settings: {
    format: 'json',
    nojsoncallback: 1,
    safe_search: 1,
    content_type: 1,
    sort: 'interestingness-desc',
    media: 'photos',
    extras: 'owner_name,url_q,url_l',
    page: 1,
    per_page: IMAGES_PER_PAGE
  }
};

export default function flickr(options) {
  let nextPage = 1;
  let images;
  let totalPages = -1;

  const settings = JSON.parse(JSON.stringify(FLICKR_API.settings));
  let apiOptions = '';

  // Compute the API options based on the settings object above
  // Each key in the settings object can be used to configure the result from Flickr API
  const computeApiOptions = function computeApiOptions() {
    apiOptions = '';
    Object.keys(settings).forEach((key) => {
      apiOptions += `&${key}=${settings[key]}`;
    });
  };

  // Function to invaidate the stored results
  // Whenever the search term is changed, the previous results need to be flushed out
  const invalidateResults = function invalidateResults() {
    images = undefined;
    totalPages = -1;
    settings.page = nextPage = 1;
  };

  // Returns the current search term
  const getSearchTerm = function getSearchTerm() {
    return settings.tags;
  };

  // Sets the current search term and invalidate the results
  const setSearchTerm = function setSearchTerm(term) {
    settings.tags = encodeURIComponent(term);
    invalidateResults();
  };

  // Function to get the current page from Flickr API
  // This function returns a promise which resolves to give the
  // array of image objects from the Flickr API
  const getPage = function getPage(pageNumber) {
    const page = parseInt(pageNumber, 10);
    if (pageNumber && !isNaN(page)) {
      settings.page = page;
      computeApiOptions();
      let apiToCall = `${FLICKR_API.url}${FLICKR_API.methods.search}&api_key=${FLICKR_API.key}${apiOptions}`;
      if (!settings.tags) {
        apiToCall = `${FLICKR_API.url}${FLICKR_API.methods.getRecent}&api_key=${FLICKR_API.key}${apiOptions}`;
      }
      return httpRequest(apiToCall).get().then((response) => {
        const result = JSON.parse(response);
        if (result.stat === 'ok') {
          totalPages = Math.min(result.photos.pages, MAX_PAGES_POSSIBLE);
          nextPage = result.photos.page + 1;
          images = result.photos.photo;
          return images;
        }
      });
    }
  };

  // Helper function to get the next page
  const getNextPage = function getNextPage() {
    return getPage(nextPage);
  };

  // Function to return the total number of pages
  // Returns a promise always. If the result was invalidated, it tries for another API request
  // if not it resolves to previously stored total pages
  const getTotalPages = function getTotalPages() {
    const promise = new Promise((resolve, reject) => {
      if (totalPages !== -1) {
        resolve(totalPages);
      } else {
        getPage(nextPage).then(() => {
          resolve(totalPages);
        }).catch((exception) => {
          reject(exception);
        });
      }
    });
    return promise;
  };

  // Function to return an array of current images in the page
  // Returns a promise always. If the result was invalidated, it tries for another API request
  // if not it resolves to previously stored images
  const getCurrentImages = function getCurrentImages() {
    const promise = new Promise((resolve, reject) => {
      if (images) {
        resolve(images);
      } else {
        getPage(nextPage).then(() => {
          resolve(images);
        }).catch((exception) => {
          reject(exception);
        });
      }
    });
    return promise;
  };

  // Helper function to get the image URL
  // from the given image object and size alphabet of Flickr API
  // For more information on relation between alphabet and size of image, visit
  // https://www.flickr.com/services/api/misc.urls.html
  const getImageUrl = function getImageUrl(imageObj, size) {
    return `https://farm${imageObj.farm}.staticflickr.com/${imageObj.server}/${imageObj.id}_${imageObj.secret}_${size}.jpg`;
  };

  // Compute the options for the flickr API initially
  computeApiOptions();
  // Set the page number
  settings.page = nextPage;
  // Set the number of images per page
  settings.per_page = (options && options.imagesPerPage) ? options.imagesPerPage : IMAGES_PER_PAGE;
  // Calcluate the number of pages possible based on the number of images above
  MAX_PAGES_POSSIBLE = parseInt((MAX_IMAGE_RESULTS / settings.per_page), 10);
  // If search term is given, then encode the serach term
  if (options && options.searchTerm) {
    settings.tags = encodeURIComponent(options.searchTerm);
  }
  return {
    getSearchTerm,
    setSearchTerm,
    getCurrentImages,
    getPage,
    getNextPage,
    getTotalPages,
    getImageUrl
  };
}
