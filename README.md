Flickr Image Viewer
===================

This application is used to view Flickr images using Flickr API. Users can also search for images and get the results by pages.

Setup
-----

1)  Clone this repo using the following command

`git clone git@bitbucket.org:sivars/flickr-image-viewer.git`


2)  Move to the repo that you have just cloned and run the following command

`npm install`

3) To start the app, run

`npm start`
  Navigate to localhost:8080 in your browser to view the app

4) This project is enabled with **ESLint**. Any JS file that you write must be linted using ESLint. To run the linting command type

`npm run lint`

5) This project uses Mocha and Chai for unit testing. To run tests, type

`npm run test`

6) To build the project for production, run

`npm run build`


Tech Stack
----------
Following is the tech stack:

 - **ES6** - The latest version of JavaScript
 - **SASS** - Using SASS files instead of plain CSS
 - **Webpack** - Module bundler. This also replaces front-end build tools like grunt or gulp
 - **Babel** - Transpile ES6 to ES5. Since the browser support for ES6 is not complete, this tool allows us to write code in ES6 which can be transpiled into ES5.
 - **ESLint** - Used to lint the JS code

Directory Structure
-------------------
 - **dist** - Contains the distributable version of the project. Ideally this folder gets pushed into the Docker or production server
 - **src** - Contains the source code of the app
 - **test** - Contains test cases for the app

src directory
-----------------
 - **scripts** - Contains the script files for the application
 - **static** - Static assets in the application
 - **styles** - SASS files for the application
 - **pwa** - Contains files related to progressive enhancement. The files in the directory must be pushed to root folder in production

Webpack
-------
This project uses webpack as its module bundler. The project consists of 2 webpack config files namely '**webpack.dev.config.js**' and '**webpack.prod.config.js**'. These files are used for dev and prod versions respectively.

The reason for choosing Webpack is, its highly extendable and easy to configure. It can replace the task runners such as Gulp, Grunt. Each file type is processed using a loader and the loading tasks can be configured. For example, before processing .js files, we can process those files using eslint loader and check for any errors and then proceed for further processing. This plug and play idea of webpack makes it extendable and easy to use.

 - In the webpack config, we have used extract text plugin to extract css. This helps in loading css in a separate file in production and also helps in caching
 - Common chunks plugin is used to extract the common pieces of code from the .js files and placing it in a separate file. This again helps in caching.
 - Copy webpack plugin is used to copy the static assets to dist folder
 - Uglify plugin is used in weback dev configuration to minify and uglify the code

**ESLint**

The project is enabled with ESLint and a production version of application can be made only if all the files pass the ESLint test.

The reason for choosing ESLint is, it enforces a common rule across the team. Developers working on this project will be forced to have uniform code styling. This helps in maintaining the project in long run.

 - The project uses babel as its parser for JS files and ES6 is enforced for all JS files
 - The project extends the ESLint rules from [**AirBnB**](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb). AirBnB is a famous and widely used standard across front-end projects.

Git Branching
-------------
The repo follows [git flow work flow](http://nvie.com/posts/a-successful-git-branching-model/). '**master**' branch is the main branch and this branch is equivalent to the code in production. '**staging**' branch is branched off from master and it allows developing new features. Usually new feature branches are branched off from staging and when done will be merged back to staging. This branch will be then pushed into staginf environment to be tested. Once the feature is ready to be shipped, staging will be merged into master.

Code Organization
-----------------

Code organization is inspired from the principle of React. The entire application is split down into small components and each component has its own mark up and style. Most of the components are independent of each other. So the components can be re-used in multiple places. Also the components are stateless, i.e the components can be rendered by passing data to them. The repo uses **adapter pattern** for components instead of the traditional class based pattern.

**Http Requests:**

All the http requests are routed through the functions from the file **src/scripts/http-request.js**. The HTTP methods like GET, PUT, POST, DELETE are wrapped around a promise and returned. Promise provides a clean interface when compared with traditional call backs. Also promises are better that [callback hell](http://callbackhell.com/) Hence the repo uses Promises.

**Flickr Module**

Flickr module from the file **src/scripts/flickr.js** is used to get images from Flickr. The important thing to note about this module is, it is **independent of DOM**. Users of this module can initialise the module with options ike search term and number of images in a page. Both these options are optional and if not given, then the module return recent images from Flickr with 10 images per page.

This module also **returns promises** for important operations like getting the images so that the users of this module can work easily. This module can be used **multiple times in the same page**. 

One important point to note is, Flickr API returns only **4000 images per query**. Hence this module cannot give more than 4000 images for a search term.

**App**

Initialization of the application occurs in the **src/scripts/app.js** file. This is the entry point for the app and this file imports all necessary modules and glues them together.

This file has the following main functionalities:
 

 - Initialising the app by gluing together multiple components
 - Reading the configuration for the app from **src/scripts/app-config.js** file and intialise the app accordingly
 - Proper loading message is shown when a user is querying for a search term
 - Proper error message shown when there is not enough result
 - Showing offline message if the user is offline
 - Making sure that DOM elements for components are created only once. i.e When the state of the app is updated, the DOM elements can be updated but not created again.

Commenting Style
----------------
The repo follows commenting style by [JSDoc](http://usejsdoc.org/). All the UI components are commented in JSDoc style.

Responsiveness
--------------
The application is responsive. It does not uses any third-party libraries like bootstrap for making it responsive.

 - Width above 900px is considered as desktop view
 - Width less than 900px is considered as mobile/tablet view
 - The lower device size limit for the application is 320px. i.e the application works well till 320px. Reason for choosing 320px is, of all the popular phones available, iPhone 5 has a width of 320px. This is the smallest width in popular phones.

CSS
---
The repo uses SASS instead of plain CSS. SASS is easy to maintain and comes with lot of options for developers.

 - [BEM naming convention](http://getbem.com/introduction/) is used for all classes
 - Eric Meyer's reset sass file is used for resetting
 - Constants for the application like theme colour, text colour are stored in **src/styles/_constants.scss**
 - Helper classes are added in **src/styles/_utils.scss**
 - Styles for UI components are added in **src/styles/components** folder
 - Styles for the pages are added in src/styles/pages. Since the app contains only one page, there is only one sass file in this directory as of now.

Unit Tests
----------
The repo uses [Mocha](https://mochajs.org/) as its testing framework and [Chai](http://chaijs.com/) as its assertion library. The test files are place is **/test** folder. The files ends with .spec.js extension.

jsDom is added as dependency for testing DOM related operations.

Accessibility
-----------
The app is navigable through keyboard. Proper tab index have been given to the element. Also the elements have aria attributes to explain their role and description.

Progressive Features
--------------------
This app uses Service workers for offline capabilities. Code related to progressive features are in **pwa** directory. Since the service workers needs to be at root level to listen to all paths, the files are placed in a separate directory and copied into root folder upon build. 

The app can behave like a stand-alone app because of the **manifest.json** file. Try visiting the app twice within a span of 5 minutes in your mobile Chrome browser. It should ask you for adding to home screen. If add to home screen option is chosen, then the app installs an icon in your mobile home screen. Then the app can be used as a stand-alone app without the need for opening mobile browser.

The app follows offline-first policy. Assets like html, js, css, font files are cached using service worker. Whenever a new request comes, files from the cache are served before going for a network request.

Road map
--------

 - Adding offline capabilities so that the app works completely offline
 - Adding a code coverage tool like Istanbul to check the code coverage
 - Adding more test cases for the UI components
 - Update the comments for all the functions in UI components
 - Update URLs of the app to have state info
 - Enable Analytics for the app
 - Get user feedback and proceed accordingly