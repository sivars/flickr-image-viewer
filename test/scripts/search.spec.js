import jsdom from 'mocha-jsdom';
import { expect } from 'chai';

import search from '../../src/scripts/search';

describe('Search Module Testing', () => {
  jsdom();
  it('search module returns an object', () => (expect(search()).be.a('object')));
});
