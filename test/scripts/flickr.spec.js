import chai, { expect } from 'chai';
import chaiAsPromised from 'chai-as-promised';

import flickr from '../../src/scripts/flickr';

chai.use(chaiAsPromised);

describe('Flickr Module Testing', () => {
  const flickrObj = flickr();
  it('Flickr module returns an object', () => (expect(flickrObj).be.a('object')));

  it('sets search term correctly', () => {
    const testSearchTerm = 'test';
    flickrObj.setSearchTerm(testSearchTerm);
    return expect(flickrObj.getSearchTerm()).equal(testSearchTerm);
  });

  it('returns a promise when get images is requested', () => {
    const result = flickrObj.getPage(1);
    return expect(result).to.be.a('promise');
  });
});
