import jsdom from 'mocha-jsdom';
import { expect } from 'chai';

import noResults from '../../src/scripts/no-results';

describe('No Results Module Testing', () => {
  jsdom();
  it('no results module returns an object', () => (expect(noResults()).be.a('object')));

  it('has a method to show loading screen', () => (expect(noResults().showNoResults).be.a('function')));

  it('has a method to hide loading screen', () => (expect(noResults().hideNoResults).be.a('function')));

  it('hides the loading screen', () => {
    const noResultsObj = noResults();
    noResultsObj.hideNoResults();
    return expect(noResultsObj.getContainerElement().style.display).equal('none');
  });

  it('shows the loading screen', () => {
    const noResultsObj = noResults();
    noResultsObj.showNoResults();
    return expect(noResultsObj.getContainerElement().style.display).equal('block');
  });
});
