import jsdom from 'mocha-jsdom';
import { expect } from 'chai';

import thumbnail from '../../src/scripts/thumbnail';

describe('Thumbnail Module Testing', () => {
  jsdom();
  it('thumbnail module returns an object', () => (expect(thumbnail()).be.a('object')));

  it('has a method to activate image', () => (expect(thumbnail().activateImage).be.a('function')));

  it('has a method to update images', () => (expect(thumbnail().updateImages).be.a('function')));

  it('properly sets the data attribute', () => {
    const URL1 = 'flickr1.com';
    const URL2 = 'flickr2.com';
    const options = {
      custom_attributes_key: ['url']
    };
    const sliderObj = thumbnail('id', [{ url: URL1 }, { url: URL2 }], 'url', options);
    const container = sliderObj.getContainerElement();
    const url = container.childNodes[1].getAttribute('data-url');
    return expect(url).equal(URL2);
  });
});
