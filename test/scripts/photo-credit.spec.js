import jsdom from 'mocha-jsdom';
import { expect } from 'chai';

import photoCredit from '../../src/scripts/photo-credit';

describe('Photo Credit Module Testing', () => {
  jsdom();
  it('photo credit module returns an object', () => (expect(photoCredit()).be.a('object')));

  it('has a method to set name', () => (expect(photoCredit().setName).be.a('function')));

  it('correctly sets the name', () => {
    const name = 'Siva';
    const photoCreditObj = photoCredit();
    photoCreditObj.setName(name);
    const container = photoCreditObj.getContainerElement();
    return expect(container.childNodes[1].innerHTML).equal(name);
  });
});
