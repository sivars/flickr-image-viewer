import jsdom from 'mocha-jsdom';
import { expect } from 'chai';

import pagination from '../../src/scripts/pagination';

describe('Pagination Module Testing', () => {
  jsdom();
  it('pagination module returns an object', () => (expect(pagination()).be.a('object')));

  it('has a method to set active button', () => (expect(pagination().setActiveButton).be.a('function')));
});
