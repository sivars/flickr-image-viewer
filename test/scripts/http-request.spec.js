import chai, { expect } from 'chai';
import chaiAsPromised from 'chai-as-promised';

import httpRequest from '../../src/scripts/http-request';

chai.use(chaiAsPromised);

describe('Http Request Module Testing', () => {
  const httpRequestObj = httpRequest();
  it('Http Request module returns an object', () => (expect(httpRequestObj).be.a('object')));

  it('has a method called get', () => (expect(httpRequestObj.get).to.be.a('function')));

  it('has a method called post', () => (expect(httpRequestObj.post).to.be.a('function')));

  it('has a method called put', () => (expect(httpRequestObj.put).to.be.a('function')));

  it('has a method called delete', () => (expect(httpRequestObj.delete).to.be.a('function')));
});
