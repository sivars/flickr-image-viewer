import jsdom from 'mocha-jsdom';
import { expect } from 'chai';

import loading from '../../src/scripts/loading';

describe('Loading Module Testing', () => {
  jsdom();
  it('loading module returns an object', () => (expect(loading()).be.a('object')));

  it('has a method to show loading screen', () => (expect(loading().showLoading).be.a('function')));

  it('has a method to hide loading screen', () => (expect(loading().hideLoading).be.a('function')));

  it('hides the loading screen', () => {
    const loadingObj = loading();
    loadingObj.hideLoading();
    return expect(loadingObj.getContainerElement().style.display).equal('none');
  });

  it('shows the loading screen', () => {
    const loadingObj = loading();
    loadingObj.showLoading();
    return expect(loadingObj.getContainerElement().style.display).equal('block');
  });
});
