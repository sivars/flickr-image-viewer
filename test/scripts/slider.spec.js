import jsdom from 'mocha-jsdom';
import { expect } from 'chai';

import slider from '../../src/scripts/slider';

describe('Slider Module Testing', () => {
  jsdom();
  it('slider module returns an object', () => (expect(slider()).be.a('object')));

  it('has a method to show image', () => (expect(slider().showImage).be.a('function')));

  it('has a method to update images', () => (expect(slider().updateImages).be.a('function')));

  it('properly sets the data attribute', () => {
    const URL1 = 'flickr1.com';
    const URL2 = 'flickr2.com';
    const options = {
      custom_attributes_key: ['url']
    };
    const sliderObj = slider('id', [{ url: URL1 }, { url: URL2 }], 'url', options);
    sliderObj.showImage(2);
    const container = sliderObj.getContainerElement();
    const url = container.childNodes[1].getAttribute('data-url');
    return expect(url).equal(URL2);
  });
});
